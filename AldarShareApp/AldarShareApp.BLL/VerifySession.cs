﻿using System.Web;
using System.Web.Mvc;

namespace AldarShareApp.BLL
{
    public sealed class VerifySession : AuthorizeAttribute
    {
        private bool IsNoPermission { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Session["ASA_User"] == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = this.IsNoPermission ? ((new UserAuthenticationController()).AcessGranted()) : ((new UserAuthenticationController()).AccessDenied());
        }

        public sealed class UserAuthenticationController : Controller
        {

            public ActionResult AccessDenied()
            {
                return RedirectToAction("Login", "Verify");
            }

            public ActionResult AcessGranted()
            {
                return RedirectToAction("Dashboard", "Home");
            }

        }

    }
}
