﻿
using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AldarShareApp.BLL.InterfaceManager
{
    public interface IUser
    {
        Task<bool> AddUser(UserRequestModel userRequestModel);
        Task<List<ParticipantCommonModel>> GetParticipantDetails(int participantID);
        Task<bool> UpdateUser(UserUpdateRequestModel userDetails);
        Task<DataTableResponseModel> GetAllUsers(DatatableRequestModel request);
    }
}
