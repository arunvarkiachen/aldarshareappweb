﻿using System.Threading.Tasks;

namespace AldarShareApp.BLL.InterfaceManager
{
    public interface IVerify
    {
        Task<bool> EndUserSession(int userID, int sessionID);
        Task<int> CreateUserSession(int User_ID);
    }
}
