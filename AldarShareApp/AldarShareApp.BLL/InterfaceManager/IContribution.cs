﻿

using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AldarShareApp.BLL.InterfaceManager
{
    public interface IContribution
    {
        Task<List<ContributionCommonModel>> GetParticipantDetails();
        Task<bool> AddContribution(ContributionRequestModel contributionRequestModel);
        Task<DataTableResponseModel> GetAllContributions(DatatableRequestModel request);
        Task<bool> DeleteContribution(int ContributionID,int UserID);
    }
}
