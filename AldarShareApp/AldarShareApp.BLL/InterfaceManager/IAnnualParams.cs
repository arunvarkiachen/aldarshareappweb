﻿

using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AldarShareApp.BLL.InterfaceManager
{
    public interface IAnnualParams
    {
        Task<bool> AddAwarding(AnnualParamsAwardingRequestModel annualAwardingRequestModel);
        Task<bool> AddVesting(AnnualParamsVestingRequestModel annualVestingRequestModel);
        Task<string> GetCalculatedShareValue(DateTime StartDate, DateTime EndDate, int Duration);
        Task<List<AnnualParamsCommonModel>> GetAwardingDetails(int AnnualParamsID);
        Task<bool> UpdateAwarding(UpdateAwardingRequestModel awardingDetails);
        Task<bool> UpdateVesting(UpdateVestingRequestModel vestingDetails);
        Task<DataTableResponseModel> GetAllAnnualParams(DatatableRequestModel request);
    }
}
