﻿using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using System.Threading.Tasks;

namespace AldarShareApp.BLL.InterfaceManager
{
    public interface ILTIProgram
    {
        Task<bool> AddLTIProgram(LTIAddRequestModel ltiAddRequestModel);
        Task<LTIGetCommonModel> GetLTIDetails(int LTIID);
        Task<bool> UpdateLTIProgram(LTIUpdateRequestModel ltiUpdateRequestModel);
        Task<DataTableResponseModel> GetAllPrograms(DatatableRequestModel request);
    }
}
