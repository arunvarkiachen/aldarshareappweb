﻿using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AldarShareApp.BLL.InterfaceManager
{
    public interface IDivident
    {
        Task<bool> AddDivident(DividentRequestModel dividentDetails);
        Task<List<DividentCommonModel>> GetDividentDetails(int DividentID);
        Task<bool> UpdateDivident(DividentUpdateRequestModel dividentDetails);
        Task<DataTableResponseModel> GetAllDividents(DatatableRequestModel request);
    }
}
