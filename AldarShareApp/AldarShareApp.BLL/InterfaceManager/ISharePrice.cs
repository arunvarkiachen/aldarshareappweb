﻿using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using System;
using System.Threading.Tasks;

namespace AldarShareApp.BLL.InterfaceManager
{
    public interface ISharePrice
    {
        Task<DataTableResponseModel> GetAllSharePrices(DateTime? start, DatatableRequestModel request, DateTime? end);
    }
}
