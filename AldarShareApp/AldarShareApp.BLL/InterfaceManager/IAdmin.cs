﻿using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AldarShareApp.BLL.InterfaceManager
{
    public interface IAdmin
    {
        Task<bool> AddAdmin(AdminRequestModel adminRequestModel);
        Task<List<AdminCommonModel>> GetAdminDetails(int AdminID);
        Task<bool> UpdateAdmin(AdminUpdateRequestModel adminDetails);
        Task<DataTableResponseModel> GetAllAdmins(DatatableRequestModel request);
    }
}
