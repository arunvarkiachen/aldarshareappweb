﻿using NLog;
using System;

namespace AldarShareApp.BLL.CommonLog
{
    public class BaseLogDetails
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static void EntryLogger(string MethodName)
        {
            logger.Trace("Entering into " + MethodName);

        }

        public static void ExitLogger(string MethodName)
        {
            logger.Trace("Leaving From  " + MethodName);

        }

        public static void CatchLogger(string MethodName, Exception ex)
        {
            logger.Error("Exception Occurred on" + MethodName + "Exception Details" + ex);

        }

        public static void ControllerEntryLogger(string ControllerName, string Environment, DateTime Date)
        {
            logger.Info("Request entered" + ControllerName + "" + Environment + "" + Date);

        }
    }
}
