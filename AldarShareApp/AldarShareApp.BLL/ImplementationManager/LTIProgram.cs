﻿using AldarShareApp.BLL.CommonLog;
using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.DAL.DBContextManager;
using AldarShareApp.DAL.Entity;
using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.BLL.ImplementationManager
{
    public class LTIProgram : ILTIProgram
    {
        #region PrivateDeclarations

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IDBRepository db { get; set; }

        #endregion

        #region LTIProgram BLL Operations

        /// <summary>
        /// Add LTI Program
        /// </summary>
        /// <param name="ltiAddRequestModel"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> AddLTIProgram(LTIAddRequestModel ltiAddRequestModel)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                LTIProgramDet programDetals = new LTIProgramDet();
                programDetals.programName = ltiAddRequestModel.ProgramTitle;
                programDetals.programYear = ltiAddRequestModel.Year;
                programDetals.createdOn = programDetals.updatedOn = DateTime.Now;
                programDetals.programStatus = ltiAddRequestModel.StatusID;
                programDetals.createdUser = programDetals.updatedUser = string.IsNullOrEmpty(ltiAddRequestModel.LoggedInUserID.ToString()) ? ltiAddRequestModel.LoggedInUserID.ToString() : "1";
                db.LTIProgramDets.Add(programDetals);
                await db.SaveChangesAsync();
                LTIProgramDet_ARC programDetals_ARC = new LTIProgramDet_ARC();
                programDetals_ARC.programName = ltiAddRequestModel.ProgramTitle;
                programDetals_ARC.programYear = ltiAddRequestModel.Year;
                programDetals_ARC.programStatus = ltiAddRequestModel.StatusID;
                programDetals_ARC.createdOn = programDetals_ARC.updatedOn = DateTime.Now;
                programDetals_ARC.comments = "Added LTI Program";
                programDetals_ARC.createdUser = programDetals_ARC.updatedUser = string.IsNullOrEmpty(ltiAddRequestModel.LoggedInUserID.ToString()) ? ltiAddRequestModel.LoggedInUserID.ToString() : "1";
                db.LTIProgramDet_ARC.Add(programDetals_ARC);
                await db.SaveChangesAsync();
                var ProgramID = db.LTIProgramDets.Where(a => a.programName == ltiAddRequestModel.ProgramTitle && a.programYear == ltiAddRequestModel.Year).FirstOrDefault().id;
                if (ProgramID != 0)
                {
                    foreach (var InnerResult in ltiAddRequestModel.SegmentsList)
                    {
                        LTIProgramSegmentDet segmentDetails = new LTIProgramSegmentDet();
                        segmentDetails.programId = ProgramID;
                        segmentDetails.segmentDescription = InnerResult.AwardingUnit;
                        segmentDetails.segmentPercentage = InnerResult.percentage;
                        segmentDetails.PaymentYear = InnerResult.PaymentYear;
                        segmentDetails.PaymentMonth = InnerResult.PaymentMonth;
                        segmentDetails.vestingYear = InnerResult.VestingYear;
                        segmentDetails.createdOn = segmentDetails.updatedOn = DateTime.Now;
                        segmentDetails.createdUser = segmentDetails.updatedUser = string.IsNullOrEmpty(ltiAddRequestModel.LoggedInUserID.ToString()) ? ltiAddRequestModel.LoggedInUserID.ToString() : "1";
                        db.LTIProgramSegmentDets.Add(segmentDetails);
                        await db.SaveChangesAsync();

                        LTIProgramSegmentDet_ARC segmentDetails_ARC = new LTIProgramSegmentDet_ARC();
                        segmentDetails_ARC.programId = ProgramID;
                        segmentDetails_ARC.segmentDescription = InnerResult.AwardingUnit;
                        segmentDetails_ARC.segmentPercentage = InnerResult.percentage;
                        segmentDetails_ARC.PaymentYear = InnerResult.PaymentYear;
                        segmentDetails_ARC.PaymentMonth = InnerResult.PaymentMonth;
                        segmentDetails_ARC.vestingYear = InnerResult.VestingYear;
                        segmentDetails_ARC.comments = "Added Segment Details";
                        segmentDetails_ARC.createdOn = segmentDetails_ARC.updatedOn = DateTime.Now;
                        segmentDetails_ARC.createdUser = segmentDetails_ARC.updatedUser = string.IsNullOrEmpty(ltiAddRequestModel.LoggedInUserID.ToString()) ? ltiAddRequestModel.LoggedInUserID.ToString() : "1";
                        db.LTIProgramSegmentDet_ARC.Add(segmentDetails_ARC);
                        await db.SaveChangesAsync();

                    }
                }

                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return true;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Get LTI Details
        /// </summary>
        /// <param name="LTIID"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<LTIGetCommonModel> GetLTIDetails(int LTIID)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                LTIGetCommonModel _LTIDetails = new LTIGetCommonModel();

                var basicDetails = await db.LTIProgramDets.Where(a => a.id == LTIID).FirstOrDefaultAsync();
                List<LTISegmentCommonModel> LTiSegment = new List<LTISegmentCommonModel>();
                var segmentDetailsList = await db.LTIProgramSegmentDets.Where(a => a.programId == LTIID).ToListAsync();
                foreach (var result in segmentDetailsList)
                {
                    LTISegmentCommonModel _baseModel = new LTISegmentCommonModel();
                    _baseModel.AwardingUnit = result.segmentDescription;
                    _baseModel.PaymentMonth = result.PaymentMonth;
                    _baseModel.PaymentYear = result.PaymentYear.HasValue ? result.PaymentYear.Value : 0;
                    _baseModel.percentage = result.segmentPercentage.HasValue ? result.segmentPercentage.Value : 0;
                    _baseModel.ProgramID = result.programId.HasValue ? result.programId.Value : 0;
                    _baseModel.VestingYear = result.vestingYear.HasValue ? result.vestingYear.Value : 0;
                    LTiSegment.Add(_baseModel);
                }
                _LTIDetails.ID = basicDetails.id;
                _LTIDetails.ProgramTitle = basicDetails.programName;
                _LTIDetails.Year = basicDetails.programYear.HasValue ? basicDetails.programYear.Value : 0;
                _LTIDetails.SegmentsList = LTiSegment;
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return _LTIDetails;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        /// <summary>
        /// Update LTI Program
        /// </summary>
        /// <param name="ltiUpdateRequestModel"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> UpdateLTIProgram(LTIUpdateRequestModel ltiUpdateRequestModel)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var lTIDetailsUpdate = await db.LTIProgramDets.Where(a => a.id == ltiUpdateRequestModel.ProgramID).FirstOrDefaultAsync();
                if (lTIDetailsUpdate != null)
                {
                    lTIDetailsUpdate.programName = ltiUpdateRequestModel.ProgramTitle;
                    lTIDetailsUpdate.programYear = ltiUpdateRequestModel.Year;
                    lTIDetailsUpdate.programStatus = ltiUpdateRequestModel.StatusID;
                    lTIDetailsUpdate.updatedUser = ltiUpdateRequestModel.LoggedInUserID.ToString();
                    lTIDetailsUpdate.updatedOn = DateTime.Now;
                    await db.SaveChangesAsync();
                }
                LTIProgramDet_ARC programDetals_ARC = new LTIProgramDet_ARC();
                programDetals_ARC.programName = ltiUpdateRequestModel.ProgramTitle;
                programDetals_ARC.programYear = ltiUpdateRequestModel.Year;
                programDetals_ARC.programStatus = ltiUpdateRequestModel.StatusID;
                programDetals_ARC.comments = "Updated program Details";
                programDetals_ARC.createdOn = programDetals_ARC.updatedOn = DateTime.Now;
                programDetals_ARC.createdUser = programDetals_ARC.updatedUser = string.IsNullOrEmpty(ltiUpdateRequestModel.LoggedInUserID.ToString()) ? ltiUpdateRequestModel.LoggedInUserID.ToString() : "1";
                db.LTIProgramDet_ARC.Add(programDetals_ARC);
                await db.SaveChangesAsync();
                var ProgramID = db.LTIProgramDets.Where(a => a.programName == ltiUpdateRequestModel.ProgramTitle && a.programYear == ltiUpdateRequestModel.Year).FirstOrDefault().id;
                if (ProgramID != 0)
                {
                    foreach (var InnerResult in ltiUpdateRequestModel.SegmentsList)
                    {
                        var lTISegmentUpdate = await db.LTIProgramSegmentDets.Where(a => a.id == InnerResult.SegmentID).FirstOrDefaultAsync();
                        if (lTISegmentUpdate != null)
                        {
                            lTISegmentUpdate.segmentDescription = InnerResult.AwardingUnit;
                            lTISegmentUpdate.segmentPercentage = InnerResult.percentage;
                            lTISegmentUpdate.PaymentMonth = InnerResult.PaymentMonth;
                            lTISegmentUpdate.PaymentYear = InnerResult.PaymentYear;
                            lTISegmentUpdate.vestingYear = InnerResult.VestingYear;
                            lTISegmentUpdate.updatedUser = ltiUpdateRequestModel.LoggedInUserID.ToString();
                            lTISegmentUpdate.updatedOn = DateTime.Now;
                            await db.SaveChangesAsync();
                        }
                        LTIProgramSegmentDet_ARC segmentDetails_ARC = new LTIProgramSegmentDet_ARC();
                        segmentDetails_ARC.programId = ltiUpdateRequestModel.ProgramID;
                        segmentDetails_ARC.segmentDescription = InnerResult.AwardingUnit;
                        segmentDetails_ARC.segmentPercentage = InnerResult.percentage;
                        segmentDetails_ARC.PaymentYear = InnerResult.PaymentYear;
                        segmentDetails_ARC.comments = "Updated program Details";
                        segmentDetails_ARC.PaymentMonth = InnerResult.PaymentMonth;
                        segmentDetails_ARC.vestingYear = InnerResult.VestingYear;
                        segmentDetails_ARC.createdOn = segmentDetails_ARC.updatedOn = DateTime.Now;
                        segmentDetails_ARC.createdUser = segmentDetails_ARC.updatedUser = string.IsNullOrEmpty(ltiUpdateRequestModel.LoggedInUserID.ToString()) ? ltiUpdateRequestModel.LoggedInUserID.ToString() : "1";
                        db.LTIProgramSegmentDet_ARC.Add(segmentDetails_ARC);
                        await db.SaveChangesAsync();
                    }
                }
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return true;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Get All Programs
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<DataTableResponseModel> GetAllPrograms(DatatableRequestModel request)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var _query = (from pd in db.LTIProgramDets
                         join sd in db.LTIProgramSegmentDets on pd.id equals sd.programId
                         select new
                         {
                             pd.programName,
                             pd.programYear,
                             sd.vestingYear,
                             sd.segmentPercentage,
                             sd.segmentDescription,
                             sd.programId,
                             sd.PaymentYear,
                             sd.PaymentMonth,
                             sd.id,
                             sd.createdOn,
                             sd.updatedOn
                         });

                if (!string.IsNullOrEmpty(request.sSearch))
                {
                    _query = _query.Where(a => a.programName.Contains(request.sSearch) || a.segmentDescription.Contains(request.sSearch));
                }
                _query = _query.OrderByDescending(a => a.programYear);
                var _count = await _query.CountAsync();
                var data = (await _query.Skip(request.iDisplayStart).Take(request.iDisplayLength).ToListAsync()).Select(r => new object[] {

                      r.PaymentMonth,
                      r.PaymentYear,
                      r.programName,
                      r.programYear,
                      r.segmentDescription,
                      r.segmentPercentage,
                      r.vestingYear,
                      r.createdOn.Value.ToString("dd MMMM, yyyy"),
                      r.updatedOn.Value.ToString("dd MMMM, yyyy"),
                      r.id
                      }).ToList();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return new DataTableResponseModel
                {
                    sEcho = request.sEcho,
                    iDisplayLength = request.iDisplayLength,
                    iTotalRecords = _count,
                    iDisplayStart = request.iDisplayStart,
                    iTotalDisplayRecords = _count,
                    aaData = data
                };

            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        #endregion
    }
}
