﻿using AldarShareApp.BLL.CommonLog;
using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.DAL.DBContextManager;
using AldarShareApp.DAL.Entity;
using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.BLL.ImplementationManager
{
    public class Admin:IAdmin
    {
        #region PrivateDeclarations

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IDBRepository db { get; set; }

        #endregion

        #region Admin BLL Operations

        /// <summary>
        /// Add Admin
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> AddAdmin(AdminRequestModel adminRequestModel)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                LTIAdminDet adminDetails = new LTIAdminDet();
                adminDetails.name = adminRequestModel.Name;
                adminDetails.username = adminRequestModel.UserName;
                adminDetails.isActive = true;
                adminDetails.createdOn = adminDetails.updatedOn = DateTime.Now;
                adminDetails.employeeCode = adminRequestModel.EmployeeCode;
                adminDetails.jobTitle = adminRequestModel.Designation;
                adminDetails.createdUser = adminDetails.updatedUser =string.IsNullOrEmpty(adminRequestModel.userID.ToString())? adminRequestModel.userID.ToString():"1"; 
                db.LTIAdminDets.Add(adminDetails);
                await db.SaveChangesAsync();
                LTIAdminDet_ARC adminDetails_ARC = new LTIAdminDet_ARC();
                adminDetails_ARC.name = adminRequestModel.Name;
                adminDetails_ARC.username = adminRequestModel.UserName;
                adminDetails_ARC.isActive = true;
                adminDetails_ARC.employeeCode = adminRequestModel.EmployeeCode;
                adminDetails_ARC.jobTitle = adminRequestModel.Designation;
                adminDetails_ARC.createdOn = adminDetails_ARC.updatedOn = DateTime.Now;
                adminDetails_ARC.comments = "Added User";
                adminDetails_ARC.createdUser = adminDetails_ARC.updatedUser = string.IsNullOrEmpty(adminRequestModel.userID.ToString()) ? adminRequestModel.userID.ToString() : "1";
                db.LTIAdminDet_ARC.Add(adminDetails_ARC);
                await db.SaveChangesAsync();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return true;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Get Admin Details
        /// </summary>
        /// <param name="AdminID"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<List<AdminCommonModel>> GetAdminDetails(int AdminID)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var adminDetails = await db.LTIAdminDets.Where(a => a.id == AdminID).Select(a => new AdminCommonModel
                {
                    Name = a.name,
                    UserName = a.username,
                    ID = a.id,
                    IsActive = a.isActive.HasValue ? a.isActive.Value : false
                }).ToListAsync();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return adminDetails;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        /// <summary>
        /// Update Admin
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> UpdateAdmin(AdminUpdateRequestModel adminDetails)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var adminDetailsUpdate = await db.LTIAdminDets.Where(a => a.id == adminDetails.AdminID).FirstOrDefaultAsync();
                if (adminDetailsUpdate != null)
                {
                    adminDetailsUpdate.name = adminDetails.Name;
                    adminDetailsUpdate.username = adminDetails.UserName;
                    adminDetailsUpdate.isActive = adminDetails.IsActive;
                    adminDetailsUpdate.updatedUser = adminDetails.LoggedInUserID.ToString();
                    adminDetailsUpdate.updatedOn = DateTime.Now;
                    adminDetailsUpdate.jobTitle = adminDetails.Designation;
                    adminDetailsUpdate.employeeCode = adminDetails.EmpCode;
                    await db.SaveChangesAsync();

                    LTIAdminDet_ARC adminDetails_ARC = new LTIAdminDet_ARC();
                    adminDetails_ARC.name = adminDetails.Name;
                    adminDetails_ARC.username = adminDetails.UserName;
                    adminDetails_ARC.isActive = adminDetails.IsActive;
                    adminDetails_ARC.comments = "Updated User";
                    adminDetails_ARC.createdOn = adminDetails_ARC.updatedOn = DateTime.Now;
                    adminDetails_ARC.jobTitle = adminDetails.Designation;
                    adminDetails_ARC.employeeCode = adminDetails.EmpCode;
                    adminDetails_ARC.createdUser = adminDetails_ARC.updatedUser = string.IsNullOrEmpty(adminDetails.LoggedInUserID.ToString()) ? adminDetails.LoggedInUserID.ToString() : "1";
                    db.LTIAdminDet_ARC.Add(adminDetails_ARC);
                    await db.SaveChangesAsync();

                    BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                    return true;
                }
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return false;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Get All Admins
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<DataTableResponseModel> GetAllAdmins(DatatableRequestModel request)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var _query = db.LTIAdminDets.Select(a => new
                {
                    a.createdOn,
                    a.updatedOn,
                    a.name,
                    a.username,
                    a.employeeCode,
                    a.id
                });
              
                if (!string.IsNullOrEmpty(request.sSearch))
                {
                    _query = _query.Where(a => a.name.Contains(request.sSearch) || a.username.Contains(request.sSearch));
                }
                switch (request.iSortCol_0)
                {
                    case 0:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.name) : _query.OrderByDescending(a => a.name);
                        break;

                    case 1:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.username) : _query.OrderByDescending(a => a.username);
                        break;

                    case 2:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.createdOn) : _query.OrderByDescending(a => a.createdOn);
                        break;

                    case 3:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.updatedOn) : _query.OrderByDescending(a => a.updatedOn);
                        break;

                    default:
                        _query = _query.OrderByDescending(a => a.id);
                        break;
                }
                var _count = await _query.CountAsync();
                var data = (await _query.Skip(request.iDisplayStart).Take(request.iDisplayLength).ToListAsync()).Select(r => new object[] {

                      r.name,
                      r.username,
                      r.createdOn.Value.ToString("dd MMMM, yyyy"),
                      r.updatedOn.Value.ToString("dd MMMM, yyyy"),
                      r.id
                      }).ToList();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return new DataTableResponseModel
                {
                    sEcho = request.sEcho,
                    iDisplayLength = request.iDisplayLength,
                    iTotalRecords = _count,
                    iDisplayStart = request.iDisplayStart,
                    iTotalDisplayRecords = _count,
                    aaData = data
                };

            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        #endregion
    }
}
