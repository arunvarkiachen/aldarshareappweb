﻿using AldarShareApp.BLL.CommonLog;
using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.DAL.DBContextManager;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AldarShareApp.BLL.ImplementationManager
{
    public class Verify : IVerify
    {
        #region Private Declarations

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IDBRepository db { get; set; }

        #endregion

        #region Authenticate

      
        #endregion

        #region Handle Session

        /// <summary>
        /// End User Session
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="sessionID"></param>
        /// <returns></returns>
        public async Task<bool> EndUserSession(int userID, int sessionID)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var _userSession = await db.UserSessions.FirstOrDefaultAsync(s => s.UserSessionKey == userID && s.UserSessionKey == sessionID);
                if (_userSession != null)
                {
                    _userSession.EndTime = DateTime.Now;
                    await db.SaveChangesAsync();
                    BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                    return true;
                }
                else
                {
                    BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                    return false;

                }

            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }


        /// <summary>
        /// Create User Session
        /// </summary>
        /// <param name="User_ID"></param>
        /// <returns></returns>
        public async Task<int> CreateUserSession(int User_ID)
        {
            try
            {
                var db = new DBRepository().Get();
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var _session = new DAL.Entity.UserSession
                {
                    UserSessionUserID = User_ID,
                    StartTime = DateTime.Now,
                    EndTime = null,
                    IsActive = true
                };
                db.UserSessions.Add(_session);
                await db.SaveChangesAsync();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return _session.UserSessionKey;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return 0;
            }
        }

        #endregion

        

    }
}
