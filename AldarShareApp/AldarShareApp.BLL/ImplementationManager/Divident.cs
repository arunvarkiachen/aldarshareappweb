﻿using AldarShareApp.BLL.CommonLog;
using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.DAL.DBContextManager;
using AldarShareApp.DAL.Entity;
using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.BLL.ImplementationManager
{
    public class Divident: IDivident
    {
        #region PrivateDeclarations

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IDBRepository db { get; set; }

        #endregion

        #region Divident BLL Operations

       /// <summary>
       /// Add Divident Details
       /// </summary>
       /// <param name="SelectedYear"></param>
       /// <param name="DivivdentValue"></param>
       /// <param name="IsForecasted"></param>
       /// <returns></returns>
        [HandleError]
        public async Task<bool> AddDivident(DividentRequestModel dividentRequestModel)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                LTIDividendDet dividentDetails = new LTIDividendDet();
                dividentDetails.ceYear = dividentRequestModel.SelectedYear;
                dividentDetails.dividend = Convert.ToDecimal(dividentRequestModel.DividentValue);
                dividentDetails.isForecasted = dividentRequestModel.IsForecasted;
                dividentDetails.createdOn = dividentDetails.updatedOn = DateTime.Now;
                dividentDetails.createdUser = dividentDetails.updatedUser = string.IsNullOrEmpty(dividentRequestModel.UserID.ToString()) ? dividentRequestModel.UserID.ToString() : "1";
                db.LTIDividendDets.Add(dividentDetails);
                await db.SaveChangesAsync();

                LTIDividendDet_ARC dividentDetails_ARC = new LTIDividendDet_ARC();
                dividentDetails_ARC.ceYear = dividentRequestModel.SelectedYear;
                dividentDetails_ARC.dividend = Convert.ToDecimal(dividentRequestModel.DividentValue);
                dividentDetails_ARC.isForecasted = dividentRequestModel.IsForecasted;
                dividentDetails_ARC.createdOn = dividentDetails_ARC.updatedOn = DateTime.Now;
                dividentDetails_ARC.comments = "Dividend Added";
                dividentDetails_ARC.createdUser = dividentDetails_ARC.updatedUser = string.IsNullOrEmpty(dividentRequestModel.UserID.ToString()) ? dividentRequestModel.UserID.ToString() : "1";
                db.LTIDividendDet_ARC.Add(dividentDetails_ARC);
                await db.SaveChangesAsync();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return true;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Get Divident Details
        /// </summary>
        /// <param name="DividentID"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<List<DividentCommonModel>> GetDividentDetails(int DividentID)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var dividentDetails = await db.LTIDividendDets.Where(a => a.id == DividentID).Select(a => new DividentCommonModel
                {
                    Divident = a.dividend.ToString(),
                    Year = a.ceYear.HasValue?a.ceYear.Value:0,
                    ID = a.id,
                    IsForecast = a.isForecasted.HasValue ? a.isForecasted.Value : false
                }).ToListAsync();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return dividentDetails;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        /// <summary>
        /// Update Admin
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> UpdateDivident(DividentUpdateRequestModel dividentDetails)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var dividentUpdateDetails = await db.LTIDividendDets.Where(a => a.id == dividentDetails.DividentID).FirstOrDefaultAsync();
                if (dividentUpdateDetails != null)
                {
                    dividentUpdateDetails.ceYear = dividentDetails.SelectedYear;
                    dividentUpdateDetails.dividend =Convert.ToDecimal(dividentDetails.DividentValue);
                    dividentUpdateDetails.isForecasted = dividentDetails.IsForecasted;
                    dividentUpdateDetails.updatedOn = DateTime.Now;
                    dividentUpdateDetails.updatedUser = dividentDetails.LoggedInUserID.ToString();
                    await db.SaveChangesAsync();

                    LTIDividendDet_ARC dividentDetails_ARC = new LTIDividendDet_ARC();
                    dividentDetails_ARC.ceYear = dividentDetails.SelectedYear;
                    dividentDetails_ARC.dividend = Convert.ToDecimal(dividentDetails.DividentValue);
                    dividentDetails_ARC.isForecasted = dividentDetails.IsForecasted;
                    dividentDetails_ARC.comments = "Dividend Updated";
                    dividentDetails_ARC.createdOn = dividentDetails_ARC.updatedOn = DateTime.Now;
                    dividentDetails_ARC.createdUser = dividentDetails_ARC.updatedUser = string.IsNullOrEmpty(dividentDetails.LoggedInUserID.ToString()) ? dividentDetails.LoggedInUserID.ToString() : "1";
                    db.LTIDividendDet_ARC.Add(dividentDetails_ARC);
                    await db.SaveChangesAsync();

                    BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                    return true;
                }
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return false;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Get All Dividents
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<DataTableResponseModel> GetAllDividents(DatatableRequestModel request)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var _query = db.LTIDividendDets.Select(a => new
                {
                    a.createdOn,
                    a.updatedOn,
                    a.ceYear,
                    a.dividend,
                    a.isForecasted,
                    a.id
                });

                if (!string.IsNullOrEmpty(request.sSearch))
                {
                    _query = _query.Where(a => a.ceYear.ToString().Contains(request.sSearch));
                }
                switch (request.iSortCol_0)
                {
                    case 0:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.ceYear) : _query.OrderByDescending(a => a.ceYear);
                        break;

                    case 1:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.dividend) : _query.OrderByDescending(a => a.dividend);
                        break;

                    case 2:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.isForecasted) : _query.OrderByDescending(a => a.isForecasted);
                        break;

                    case 3:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.createdOn) : _query.OrderByDescending(a => a.createdOn);
                        break;

                    case 4:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.updatedOn) : _query.OrderByDescending(a => a.updatedOn);
                        break;

                    default:
                        _query = _query.OrderByDescending(a => a.id);
                        break;
                }
                var _count = await _query.CountAsync();
                var data = (await _query.Skip(request.iDisplayStart).Take(request.iDisplayLength).ToListAsync()).Select(r => new object[] {

                      r.ceYear,
                      r.dividend,
                      r.isForecasted,
                      r.createdOn.Value.ToString("dd MMMM, yyyy"),
                      r.updatedOn.Value.ToString("dd MMMM, yyyy"),
                      r.id
                      }).ToList();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return new DataTableResponseModel
                {
                    sEcho = request.sEcho,
                    iDisplayLength = request.iDisplayLength,
                    iTotalRecords = _count,
                    iDisplayStart = request.iDisplayStart,
                    iTotalDisplayRecords = _count,
                    aaData = data
                };

            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        #endregion

    }
}
