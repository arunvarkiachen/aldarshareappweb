﻿using AldarShareApp.BLL.CommonLog;
using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.DAL.DBContextManager;
using AldarShareApp.DAL.Entity;
using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.BLL.ImplementationManager
{
    public class User : IUser
    {
        #region PrivateDeclarations

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IDBRepository db { get; set; }

        #endregion

        #region User BLL Operations

        /// <summary>
        /// Add User
        /// </summary>
        /// <param name="userRequestModel"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> AddUser(UserRequestModel userRequestModel)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                LTIParticipantDet participantDetails = new LTIParticipantDet();
                participantDetails.employeeName = userRequestModel.Name;
                participantDetails.userName = userRequestModel.UserName;
                participantDetails.employeeCode = userRequestModel.EmployeeCode;
                participantDetails.designation = userRequestModel.Designation;
                participantDetails.participationStartDate = userRequestModel.ParticipationStartDate;
                participantDetails.createdOn = participantDetails.updatedOn = DateTime.Now;
                participantDetails.createdUser = participantDetails.updatedUser = string.IsNullOrEmpty(userRequestModel.UserID.ToString()) ? userRequestModel.UserID.ToString() : "1";
                db.LTIParticipantDets.Add(participantDetails);
                await db.SaveChangesAsync();
                LTIParticipantDet_ARC participantDetails_ARC = new LTIParticipantDet_ARC();
                participantDetails_ARC.employeeName = userRequestModel.Name;
                participantDetails_ARC.userName = userRequestModel.UserName;
                participantDetails_ARC.employeeCode = userRequestModel.EmployeeCode;
                participantDetails_ARC.designation = userRequestModel.Designation;
                participantDetails_ARC.comments = "Add User";
                participantDetails_ARC.participationStartDate = userRequestModel.ParticipationStartDate;
                participantDetails_ARC.createdOn = participantDetails_ARC.updatedOn = DateTime.Now;
                participantDetails_ARC.createdUser = participantDetails_ARC.updatedUser = string.IsNullOrEmpty(userRequestModel.UserID.ToString()) ? userRequestModel.UserID.ToString() : "1";
                db.LTIParticipantDet_ARC.Add(participantDetails_ARC);
                await db.SaveChangesAsync();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return true;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Get Participant Details
        /// </summary>
        /// <param name="participantID"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<List<ParticipantCommonModel>> GetParticipantDetails(int participantID)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var participantDetails = await db.LTIParticipantDets.Where(a => a.id == participantID).Select(a => new ParticipantCommonModel
                {
                    Name = a.employeeName,
                    UserName = a.userName,
                    ID = a.id,
                    IsActiveParticipant = a.isEndedParticipation.HasValue ? a.isEndedParticipation.Value : false,
                    Designation = a.designation,
                    EmployeeCode = a.employeeCode,
                    ParticipationStartDate = a.participationStartDate.HasValue ? a.participationStartDate.Value : new DateTime()
                }).ToListAsync();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return participantDetails;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="userDetails"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> UpdateUser(UserUpdateRequestModel userDetails)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var userDetailsUpdate = await db.LTIParticipantDets.Where(a => a.id == userDetails.ParticipantID).FirstOrDefaultAsync();
                if (userDetailsUpdate != null)
                {
                    userDetailsUpdate.employeeName = userDetails.Name;
                    userDetailsUpdate.userName = userDetails.UserName;
                    userDetailsUpdate.employeeCode = userDetails.EmployeeCode;
                    userDetailsUpdate.designation = userDetails.Designation;
                    userDetailsUpdate.participationStartDate = userDetails.ParticipationStartDate;
                    userDetailsUpdate.participationEndDate = userDetails.ParticipationEndDate;
                    userDetailsUpdate.isEndedParticipation = userDetails.IsEndedParticipation;
                    userDetailsUpdate.updatedUser = userDetails.UserID.ToString();
                    userDetailsUpdate.updatedOn = DateTime.Now;
                    await db.SaveChangesAsync();

                    LTIParticipantDet_ARC adminDetails_ARC = new LTIParticipantDet_ARC();
                    adminDetails_ARC.employeeName = userDetails.Name;
                    adminDetails_ARC.userName = userDetails.UserName;
                    adminDetails_ARC.employeeCode = userDetails.EmployeeCode;
                    adminDetails_ARC.designation = userDetails.Designation;
                    adminDetails_ARC.isEndedParticipation = userDetails.IsEndedParticipation;
                    adminDetails_ARC.participationStartDate = userDetails.ParticipationStartDate;
                    adminDetails_ARC.participationEndDate = userDetails.ParticipationEndDate;
                    adminDetails_ARC.createdOn = adminDetails_ARC.updatedOn = DateTime.Now;
                    adminDetails_ARC.comments = "Updated User";
                    adminDetails_ARC.createdUser = adminDetails_ARC.updatedUser = string.IsNullOrEmpty(userDetails.UserID.ToString()) ? userDetails.UserID.ToString() : "1";
                    db.LTIParticipantDet_ARC.Add(adminDetails_ARC);
                    await db.SaveChangesAsync();

                    BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                    return true;
                }
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return false;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Get All Users
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<DataTableResponseModel> GetAllUsers(DatatableRequestModel request)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var _query = db.LTIParticipantDets.Select(a => new
                {
                    a.createdOn,
                    a.updatedOn,
                    a.userName,
                    a.employeeName,
                    a.employeeCode,
                    a.designation,
                    a.isEndedParticipation,
                    a.participationStartDate,
                    a.participationEndDate,
                    a.id
                });

                if (!string.IsNullOrEmpty(request.sSearch))
                {
                    _query = _query.Where(a => a.employeeName.Contains(request.sSearch)  || a.designation.Contains(request.sSearch));
                }
                switch (request.iSortCol_0)
                {
                    case 0:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.employeeName) : _query.OrderByDescending(a => a.employeeName);
                        break;

                    case 1:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.userName) : _query.OrderByDescending(a => a.userName);
                        break;

                    case 2:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.employeeCode) : _query.OrderByDescending(a => a.employeeCode);
                        break;

                    case 3:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.designation) : _query.OrderByDescending(a => a.designation);
                        break;

                    case 4:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.isEndedParticipation) : _query.OrderByDescending(a => a.isEndedParticipation);
                        break;

                    case 5:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.participationStartDate) : _query.OrderByDescending(a => a.participationStartDate);
                        break;

                    case 6:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.participationEndDate) : _query.OrderByDescending(a => a.participationEndDate);
                        break;

                    case 7:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.updatedOn) : _query.OrderByDescending(a => a.updatedOn);
                        break;

                    default:
                        _query = _query.OrderByDescending(a => a.createdOn);
                        break;
                }
                var _count = await _query.CountAsync();
                if(!string.IsNullOrEmpty(request.sSearch))
                {
                    request.iDisplayStart = 0;
                }
                var data = (await _query.Skip(request.iDisplayStart).Take(request.iDisplayLength).ToListAsync()).Select(r => new object[] {

                      r.employeeName,
                      r.userName,
                      r.employeeCode,
                      r.designation,
                      r.isEndedParticipation.HasValue?r.isEndedParticipation.Value:false,
                      r.participationStartDate.Value.ToString("dd MMMM, yyyy"),
                      r.participationEndDate.HasValue? r.participationEndDate.Value.ToString("dd MMMM, yyyy"):"-",
                      r.updatedOn.Value.ToString("dd MMMM, yyyy"),
                      r.id
                      }).ToList();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return new DataTableResponseModel
                {
                    sEcho = request.sEcho,
                    iDisplayLength = request.iDisplayLength,
                    iTotalRecords = _count,
                    iDisplayStart = request.iDisplayStart,
                    iTotalDisplayRecords = _count,
                    aaData = data
                };

            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        #endregion

    }
}
