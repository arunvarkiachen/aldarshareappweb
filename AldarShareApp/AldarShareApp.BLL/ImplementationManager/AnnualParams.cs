﻿using AldarShareApp.BLL.CommonLog;
using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.DAL.DBContextManager;
using AldarShareApp.DAL.Entity;
using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.BLL.ImplementationManager
{
    public class AnnualParams : IAnnualParams
    {
        #region PrivateDeclarations

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IDBRepository db { get; set; }

        #endregion

        #region AnnualParams BLL Operations


        /// <summary>
        /// Add Awarding
        /// </summary>
        /// <param name="annualAwardingRequestModel"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> AddAwarding(AnnualParamsAwardingRequestModel annualAwardingRequestModel)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                if (annualAwardingRequestModel.IsAwardingForward == true)
                {
                    annualAwardingRequestModel.AwrdingSharePrice = await GetCalculatedShareValue(annualAwardingRequestModel.AwardingStartDate, annualAwardingRequestModel.AwardingEndDate, annualAwardingRequestModel.Duration);
                }
                else
                {
                    annualAwardingRequestModel.AwrdingSharePrice = await GetCalculatedShareValue(annualAwardingRequestModel.AwardingEndDate, annualAwardingRequestModel.AwardingStartDate, annualAwardingRequestModel.Duration);
                }
                if (annualAwardingRequestModel.AwrdingSharePrice != "0")
                {
                    LTIAnnualParamsDet awardingDetails = new LTIAnnualParamsDet();
                    awardingDetails.ceYear = annualAwardingRequestModel.year;
                    awardingDetails.awardingStartDate = annualAwardingRequestModel.AwardingStartDate;
                    awardingDetails.awardingEndDate = annualAwardingRequestModel.AwardingEndDate;
                    awardingDetails.createdOn = awardingDetails.updatedOn = DateTime.Now;
                    awardingDetails.vestingSharePrice = Convert.ToDecimal(annualAwardingRequestModel.AwrdingSharePrice);
                    awardingDetails.createdUser = awardingDetails.updatedUser = string.IsNullOrEmpty(annualAwardingRequestModel.LoggedInUserID.ToString()) ? annualAwardingRequestModel.LoggedInUserID.ToString() : "1";
                    db.LTIAnnualParamsDets.Add(awardingDetails);
                    await db.SaveChangesAsync();
                    LTIAnnualParamsDet_ARC awardingDetails_ARC = new LTIAnnualParamsDet_ARC();
                    awardingDetails_ARC.ceYear = annualAwardingRequestModel.year;
                    awardingDetails_ARC.awardingStartDate = annualAwardingRequestModel.AwardingStartDate;
                    awardingDetails_ARC.awardingEndDate = annualAwardingRequestModel.AwardingEndDate;
                    awardingDetails_ARC.vestingSharePrice = Convert.ToDecimal(annualAwardingRequestModel.AwrdingSharePrice);
                    awardingDetails_ARC.createdOn = awardingDetails_ARC.updatedOn = DateTime.Now;
                    awardingDetails_ARC.comments = "Add Awarding";
                    awardingDetails_ARC.createdUser = awardingDetails_ARC.updatedUser = string.IsNullOrEmpty(annualAwardingRequestModel.LoggedInUserID.ToString()) ? annualAwardingRequestModel.LoggedInUserID.ToString() : "1";
                    db.LTIAnnualParamsDet_ARC.Add(awardingDetails_ARC);
                    await db.SaveChangesAsync();
                    BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Add Vesting
        /// </summary>
        /// <param name="annualVestingRequestModel"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> AddVesting(AnnualParamsVestingRequestModel annualVestingRequestModel)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                annualVestingRequestModel.VestingSharePrice = "";
                if (annualVestingRequestModel.IsForecastedVesting == false && annualVestingRequestModel.IsForward == true)
                {
                    annualVestingRequestModel.VestingSharePrice = await GetCalculatedShareValue(annualVestingRequestModel.VestingStartDate, annualVestingRequestModel.VestingEndDate, annualVestingRequestModel.Duration);
                }
                else if (annualVestingRequestModel.IsForecastedVesting == true)
                {
                    annualVestingRequestModel.VestingSharePrice = annualVestingRequestModel.ForecastValue;
                }
                else if (annualVestingRequestModel.IsForecastedVesting == false && annualVestingRequestModel.IsForward == false)
                {
                    annualVestingRequestModel.VestingSharePrice = await GetCalculatedShareValue(annualVestingRequestModel.VestingEndDate, annualVestingRequestModel.VestingStartDate, annualVestingRequestModel.Duration);
                }
                if (annualVestingRequestModel.VestingSharePrice != "0")
                {
                    LTIAnnualParamsDet awardingDetails = new LTIAnnualParamsDet();
                    awardingDetails.ceYear = annualVestingRequestModel.Year;
                    awardingDetails.vestingStartDate = annualVestingRequestModel.VestingStartDate;
                    awardingDetails.vestingEndDate = annualVestingRequestModel.VestingEndDate;
                    awardingDetails.vestingSharePrice = Convert.ToDecimal(annualVestingRequestModel.VestingSharePrice);
                    awardingDetails.createdOn = awardingDetails.updatedOn = DateTime.Now;
                    awardingDetails.createdUser = awardingDetails.updatedUser = string.IsNullOrEmpty(annualVestingRequestModel.LoggedInUserID.ToString()) ? annualVestingRequestModel.LoggedInUserID.ToString() : "1";
                    db.LTIAnnualParamsDets.Add(awardingDetails);
                    await db.SaveChangesAsync();
                    LTIAnnualParamsDet_ARC awardingDetails_ARC = new LTIAnnualParamsDet_ARC();
                    awardingDetails_ARC.ceYear = annualVestingRequestModel.Year;
                    awardingDetails_ARC.awardingStartDate = annualVestingRequestModel.VestingStartDate;
                    awardingDetails_ARC.awardingEndDate = annualVestingRequestModel.VestingEndDate;
                    awardingDetails_ARC.vestingSharePrice = Convert.ToDecimal(annualVestingRequestModel.VestingSharePrice);
                    awardingDetails_ARC.createdOn = awardingDetails_ARC.updatedOn = DateTime.Now;
                    awardingDetails_ARC.comments = "Vesting Added";
                    awardingDetails_ARC.createdUser = awardingDetails_ARC.updatedUser = string.IsNullOrEmpty(annualVestingRequestModel.LoggedInUserID.ToString()) ? annualVestingRequestModel.LoggedInUserID.ToString() : "1";
                    db.LTIAnnualParamsDet_ARC.Add(awardingDetails_ARC);
                    await db.SaveChangesAsync();
                    BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Calculation To Find Out Share Value
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="Duration"></param>
        /// <returns></returns>
        public async Task<string> GetCalculatedShareValue(DateTime StartDate, DateTime EndDate, int Duration)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var sumValue = await db.LTISharePriceDets.Where(a => a.shareDate >= StartDate && a.shareDate <= EndDate).SumAsync(a => a.sharePrice);
                if (sumValue != 0)
                {
                    sumValue = sumValue / Duration;
                }
                return sumValue.ToString();
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        /// <summary>
        /// Get Annual Params Details
        /// </summary>
        /// <param name="AnnualParamsID"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<List<AnnualParamsCommonModel>> GetAwardingDetails(int AnnualParamsID)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var annualParamsDetails = await db.LTIAnnualParamsDets.Where(a => a.id == AnnualParamsID).Select(a => new AnnualParamsCommonModel
                {
                    Year = a.ceYear.HasValue ? a.ceYear.Value : 0,
                    AwardingStartDate = a.awardingStartDate.HasValue ? a.awardingStartDate.Value : DateTime.MinValue,
                    AwardingEndDate = a.awardingEndDate.HasValue ? a.awardingEndDate.Value : DateTime.MinValue,
                    AwardingShareValue = Convert.ToString(a.awardingSharePrice),
                    VestingStartDate = a.vestingStartDate.HasValue ? a.vestingStartDate.Value : DateTime.MinValue,
                    VestingEndDate = a.vestingEndDate.HasValue ? a.vestingEndDate.Value : DateTime.MinValue,
                    VestingShareValue = Convert.ToString(a.vestingSharePrice),
                    isForcastedVSP = a.isForcastedVSP.HasValue ? a.isForcastedVSP.Value : false,
                    ID = a.id
                }).ToListAsync();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return annualParamsDetails;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        /// <summary>
        /// Update Awarding
        /// </summary>
        /// <param name="awardingDetails"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> UpdateAwarding(UpdateAwardingRequestModel awardingDetails)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                if (awardingDetails.IsAwardingForward == true)
                {
                    awardingDetails.AwrdingSharePrice = await GetCalculatedShareValue(awardingDetails.AwardingStartDate, awardingDetails.AwardingEndDate, awardingDetails.Duration);
                }
                else
                {
                    awardingDetails.AwrdingSharePrice = await GetCalculatedShareValue(awardingDetails.AwardingEndDate, awardingDetails.AwardingStartDate, awardingDetails.Duration);
                }
                var awardingDetailsUpdate = await db.LTIAnnualParamsDets.Where(a => a.id == awardingDetails.AwardID).FirstOrDefaultAsync();
                if (awardingDetailsUpdate != null)
                {
                    awardingDetailsUpdate.awardingEndDate = awardingDetails.AwardingEndDate;
                    awardingDetailsUpdate.awardingStartDate = awardingDetails.AwardingStartDate;
                    awardingDetailsUpdate.ceYear = awardingDetails.year;
                    awardingDetailsUpdate.awardingSharePrice = Convert.ToDecimal(awardingDetails.AwrdingSharePrice);
                    awardingDetailsUpdate.updatedUser = awardingDetails.LoggedInUserID.ToString();
                    awardingDetailsUpdate.updatedOn = DateTime.Now;
                    await db.SaveChangesAsync();

                    LTIAnnualParamsDet_ARC awardingDetails_ARC = new LTIAnnualParamsDet_ARC();
                    awardingDetails_ARC.ceYear = awardingDetails.year;
                    awardingDetails_ARC.awardingStartDate = awardingDetails.AwardingStartDate;
                    awardingDetails_ARC.awardingEndDate = awardingDetails.AwardingEndDate;
                    awardingDetails_ARC.vestingSharePrice = Convert.ToDecimal(awardingDetails.AwrdingSharePrice);
                    awardingDetails_ARC.createdOn = awardingDetails_ARC.updatedOn = DateTime.Now;
                    awardingDetails_ARC.comments = "Update Awarding";
                    awardingDetails_ARC.createdUser = awardingDetails_ARC.updatedUser = string.IsNullOrEmpty(awardingDetails.LoggedInUserID.ToString()) ? awardingDetails.LoggedInUserID.ToString() : "1";
                    db.LTIAnnualParamsDet_ARC.Add(awardingDetails_ARC);
                    await db.SaveChangesAsync();

                    BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                    return true;
                }
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return false;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Update Vesting
        /// </summary>
        /// <param name="vestingDetails"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> UpdateVesting(UpdateVestingRequestModel vestingDetails)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                vestingDetails.VestingSharePrice = "";
                if (vestingDetails.IsForecastedVesting == false && vestingDetails.IsForward == true)
                {
                    vestingDetails.VestingSharePrice = await GetCalculatedShareValue(vestingDetails.VestingStartDate, vestingDetails.VestingEndDate, vestingDetails.Duration);
                }
                else if (vestingDetails.IsForecastedVesting == true)
                {
                    vestingDetails.VestingSharePrice = vestingDetails.ForecastValue;
                }
                else if (vestingDetails.IsForecastedVesting == false && vestingDetails.IsForward == false)
                {
                    vestingDetails.VestingSharePrice = await GetCalculatedShareValue(vestingDetails.VestingEndDate, vestingDetails.VestingStartDate, vestingDetails.Duration);
                }
                var awardingDetailsUpdate = await db.LTIAnnualParamsDets.Where(a => a.id == vestingDetails.VestingID).FirstOrDefaultAsync();
                if (awardingDetailsUpdate != null)
                {
                    awardingDetailsUpdate.vestingEndDate = vestingDetails.VestingEndDate;
                    awardingDetailsUpdate.vestingStartDate = vestingDetails.VestingStartDate;
                    awardingDetailsUpdate.ceYear = vestingDetails.Year;
                    awardingDetailsUpdate.vestingSharePrice = Convert.ToDecimal(vestingDetails.VestingSharePrice);
                    awardingDetailsUpdate.updatedUser = vestingDetails.LoggedInUserID.ToString();
                    awardingDetailsUpdate.updatedOn = DateTime.Now;
                    await db.SaveChangesAsync();

                    LTIAnnualParamsDet_ARC awardingDetails_ARC = new LTIAnnualParamsDet_ARC();
                    awardingDetails_ARC.ceYear = vestingDetails.Year;
                    awardingDetails_ARC.awardingStartDate = vestingDetails.VestingStartDate;
                    awardingDetails_ARC.awardingEndDate = vestingDetails.VestingEndDate;
                    awardingDetails_ARC.vestingSharePrice = Convert.ToDecimal(vestingDetails.VestingSharePrice);
                    awardingDetails_ARC.createdOn = awardingDetails_ARC.updatedOn = DateTime.Now;
                    awardingDetails_ARC.comments = "Vesting Updated";
                    awardingDetails_ARC.createdUser = awardingDetails_ARC.updatedUser = string.IsNullOrEmpty(vestingDetails.LoggedInUserID.ToString()) ? vestingDetails.LoggedInUserID.ToString() : "1";
                    db.LTIAnnualParamsDet_ARC.Add(awardingDetails_ARC);
                    await db.SaveChangesAsync();

                    BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                    return true;
                }
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return false;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Get All Admins
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<DataTableResponseModel> GetAllAnnualParams(DatatableRequestModel request)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var _query = db.LTIAnnualParamsDets.Select(a => new
                {
                    a.createdOn,
                    a.updatedOn,
                    a.ceYear,
                    a.awardingSharePrice,
                    a.vestingSharePrice,
                    a.id
                });

                if (!string.IsNullOrEmpty(request.sSearch))
                {
                    _query = _query.Where(a => a.ceYear.Value.ToString().Contains(request.sSearch));
                }
                switch (request.iSortCol_0)
                {
                    case 0:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.ceYear) : _query.OrderByDescending(a => a.ceYear);
                        break;

                    case 1:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.awardingSharePrice) : _query.OrderByDescending(a => a.awardingSharePrice);
                        break;

                    case 2:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.vestingSharePrice) : _query.OrderByDescending(a => a.vestingSharePrice);
                        break;

                    case 3:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.createdOn) : _query.OrderByDescending(a => a.createdOn);
                        break;

                    case 4:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.updatedOn) : _query.OrderByDescending(a => a.updatedOn);
                        break;

                    default:
                        _query = _query.OrderByDescending(a => a.id);
                        break;
                }
                var _count = await _query.CountAsync();
                var data = (await _query.Skip(request.iDisplayStart).Take(request.iDisplayLength).ToListAsync()).Select(r => new object[] {

                      r.ceYear,
                      r.awardingSharePrice,
                      r.vestingSharePrice,
                      r.createdOn.Value.ToString("dd MMMM, yyyy"),
                      r.updatedOn.Value.ToString("dd MMMM, yyyy"),
                      r.id
                      }).ToList();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return new DataTableResponseModel
                {
                    sEcho = request.sEcho,
                    iDisplayLength = request.iDisplayLength,
                    iTotalRecords = _count,
                    iDisplayStart = request.iDisplayStart,
                    iTotalDisplayRecords = _count,
                    aaData = data
                };

            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }


        #endregion
    }
}
