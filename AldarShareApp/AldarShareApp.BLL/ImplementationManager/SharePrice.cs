﻿using AldarShareApp.BLL.CommonLog;
using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.DAL.DBContextManager;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AldarShareApp.BLL.ImplementationManager
{
    public class SharePrice:ISharePrice
    {
        #region PrivateDeclarations

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IDBRepository db { get; set; }

        #endregion

        #region SharePrice BLL Operations

        /// <summary>
        /// Get All Share Prices
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<DataTableResponseModel> GetAllSharePrices(DateTime? start, DatatableRequestModel request, DateTime? end)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var _query = db.LTISharePriceDets.Select(a => new
                {
                    a.createdOn,
                    a.updatedOn,
                    a.shareDate,
                    a.sharePrice,
                    a.currency,
                    a.id
                });
                if (start != null && end != null)
                {
                    end = end.Value.AddDays(1);
                    _query = _query.Where(a => a.createdOn >= start.Value && a.createdOn <= end.Value);
                }
                if (!string.IsNullOrEmpty(request.sSearch))
                {
                    _query = _query.Where(a => a.currency.Contains(request.sSearch));
                }
                switch (request.iSortCol_0)
                {
                    case 0:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.shareDate) : _query.OrderByDescending(a => a.shareDate);
                        break;

                    case 1:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.sharePrice) : _query.OrderByDescending(a => a.sharePrice);
                        break;

                    case 2:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.currency) : _query.OrderByDescending(a => a.currency);
                        break;

                    case 3:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.createdOn) : _query.OrderByDescending(a => a.createdOn);
                        break;

                    case 4:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.updatedOn) : _query.OrderByDescending(a => a.updatedOn);
                        break;

                    default:
                        _query = _query.OrderByDescending(a => a.id);
                        break;
                }
                var _count = await _query.CountAsync();
                var data = (await _query.Skip(request.iDisplayStart).Take(request.iDisplayLength).ToListAsync()).Select(r => new object[] {

                      r.shareDate,
                      r.sharePrice,
                      r.currency,
                      r.createdOn.Value.ToString("dd MMMM, yyyy"),
                      r.updatedOn.Value.ToString("dd MMMM, yyyy"),
                      r.id
                      }).ToList();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return new DataTableResponseModel
                {
                    sEcho = request.sEcho,
                    iDisplayLength = request.iDisplayLength,
                    iTotalRecords = _count,
                    iDisplayStart = request.iDisplayStart,
                    iTotalDisplayRecords = _count,
                    aaData = data
                };

            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        #endregion
    }
}
