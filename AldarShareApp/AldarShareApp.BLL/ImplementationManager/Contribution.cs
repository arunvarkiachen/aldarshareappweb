﻿

using AldarShareApp.BLL.CommonLog;
using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.DAL.DBContextManager;
using AldarShareApp.DAL.Entity;
using AldarShareApp.Models.CommonModel;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.BLL.ImplementationManager
{
    public class Contribution : IContribution
    {
        #region PrivateDeclarations

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IDBRepository db { get; set; }

        #endregion

        #region Contribution BLL Operations

        /// <summary>
        /// Get All Employees
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [HandleError]
        public async Task<List<ContributionCommonModel>> GetParticipantDetails()
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var contributionDetails = await db.LTIParticipantDets.Select(a => new ContributionCommonModel
                {
                    participantName = a.employeeName,
                    ParticipantID = a.employeeCode
                }).ToListAsync();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return contributionDetails;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        /// <summary>
        /// AddContribution
        /// </summary>
        /// <param name="contributionRequestModel"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> AddContribution(ContributionRequestModel contributionRequestModel)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                decimal SharesAwarded = 0;
                var ContributionAmount = (Convert.ToDecimal(contributionRequestModel.Bonus) / ((contributionRequestModel.EmpPercentage + contributionRequestModel.CompPercentage) / 100));
                if (ContributionAmount != 0)
                {
                    SharesAwarded = ContributionAmount / contributionRequestModel.CurrentShareValue;
                }
                LTIAwardingDet contributionDetails = new LTIAwardingDet();
                contributionDetails.ceYear = contributionRequestModel.Year;
                contributionDetails.programId = contributionRequestModel.PID;
                contributionDetails.id = contributionRequestModel.EmpID;
                contributionDetails.bonusAmount = Convert.ToDecimal(contributionRequestModel.Bonus);
                contributionDetails.employeeContributionPerc = contributionRequestModel.EmpPercentage;
                contributionDetails.companyContributionPerc = contributionRequestModel.CompPercentage;
                contributionDetails.awardingSharePrice = contributionRequestModel.CurrentShareValue;
                contributionDetails.totalContributionAmount = ContributionAmount;
                contributionDetails.sharesAwarded = Convert.ToInt32(SharesAwarded);
                contributionDetails.createdOn = contributionDetails.updatedOn = DateTime.Now;
                contributionDetails.createdUser = contributionDetails.updatedUser = string.IsNullOrEmpty(contributionRequestModel.UserID.ToString()) ? contributionRequestModel.UserID.ToString() : "1";
                db.LTIAwardingDets.Add(contributionDetails);
                await db.SaveChangesAsync();
                LTIAwardingDet_ARC contributionDetails_ARC = new LTIAwardingDet_ARC();
                contributionDetails_ARC.ceYear = contributionRequestModel.Year;
                contributionDetails_ARC.programId = contributionRequestModel.PID;
                contributionDetails_ARC.id = contributionRequestModel.EmpID;
                contributionDetails_ARC.bonusAmount = Convert.ToDecimal(contributionRequestModel.Bonus);
                contributionDetails_ARC.employeeContributionPerc = contributionRequestModel.EmpPercentage;
                contributionDetails_ARC.companyContributionPerc = contributionRequestModel.CompPercentage;
                contributionDetails_ARC.awardingSharePrice = contributionRequestModel.CurrentShareValue;
                contributionDetails_ARC.totalContributionAmount = ContributionAmount;
                contributionDetails_ARC.comments = "Added Contribution";
                contributionDetails_ARC.sharesAwarded = Convert.ToInt32(SharesAwarded);
                contributionDetails_ARC.createdOn = contributionDetails_ARC.updatedOn = DateTime.Now;
                contributionDetails_ARC.createdUser = contributionDetails_ARC.updatedUser = string.IsNullOrEmpty(contributionRequestModel.UserID.ToString()) ? contributionRequestModel.UserID.ToString() : "1";
                db.LTIAwardingDet_ARC.Add(contributionDetails_ARC);
                await db.SaveChangesAsync();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return true;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        /// <summary>
        /// Get All Contributions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<DataTableResponseModel> GetAllContributions(DatatableRequestModel request)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var _query = (from pd in db.LTIParticipantDets
                              join ad in db.LTIAwardingDets on pd.id equals ad.employeeId
                              select new
                              {
                                  ad.createdOn,
                                  ad.updatedOn,
                                  pd.employeeName,
                                  ad.bonusAmount,
                                  ad.employeeContributionPerc,
                                  ad.companyContributionPerc,
                                  ad.totalContributionAmount,
                                  ad.sharesAwarded,
                                  ad.id
                              });


                if (!string.IsNullOrEmpty(request.sSearch))
                {
                    _query = _query.Where(a => a.employeeName.Contains(request.sSearch));
                }
                switch (request.iSortCol_0)
                {
                    case 0:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.employeeName) : _query.OrderByDescending(a => a.employeeName);
                        break;

                    case 1:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.bonusAmount) : _query.OrderByDescending(a => a.bonusAmount);
                        break;

                    case 2:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.employeeContributionPerc) : _query.OrderByDescending(a => a.employeeContributionPerc);
                        break;

                    case 3:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.companyContributionPerc) : _query.OrderByDescending(a => a.companyContributionPerc);
                        break;

                    case 4:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.totalContributionAmount) : _query.OrderByDescending(a => a.totalContributionAmount);
                        break;

                    case 5:
                        _query = request.sSortDir_0 == "asc" ? _query.OrderBy(a => a.sharesAwarded) : _query.OrderByDescending(a => a.sharesAwarded);
                        break;

                    default:
                        _query = _query.OrderByDescending(a => a.id);
                        break;
                }
                var _count = await _query.CountAsync();
                var data = (await _query.Skip(request.iDisplayStart).Take(request.iDisplayLength).ToListAsync()).Select(r => new object[] {

                      r.employeeName,
                      r.bonusAmount,
                      r.employeeContributionPerc,
                      r.companyContributionPerc,
                      r.totalContributionAmount,
                      r.sharesAwarded,
                      r.id
                      }).ToList();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                return new DataTableResponseModel
                {
                    sEcho = request.sEcho,
                    iDisplayLength = request.iDisplayLength,
                    iTotalRecords = _count,
                    iDisplayStart = request.iDisplayStart,
                    iTotalDisplayRecords = _count,
                    aaData = data
                };

            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        /// <summary>
        /// Delete Contribution
        /// </summary>
        /// <param name="ContributionID"></param>
        /// <returns></returns>
        [HandleError]
        public async Task<bool> DeleteContribution(int ContributionID,int UserID)
        {
            try
            {
                BaseLogDetails.EntryLogger(MethodBase.GetCurrentMethod().Name);
                var db = new DBRepository().Get();
                var contributionDetails = await db.LTIAwardingDets.Where(a => a.id == ContributionID).FirstOrDefaultAsync();
                BaseLogDetails.ExitLogger(MethodBase.GetCurrentMethod().Name);
                if (contributionDetails != null)
                {
                    db.LTIAwardingDets.Remove(contributionDetails);
                    await db.SaveChangesAsync();
                    return true;
                }
                
                return false;
            }
            catch (Exception ex)
            {
                BaseLogDetails.CatchLogger(MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        #endregion

    }
}
