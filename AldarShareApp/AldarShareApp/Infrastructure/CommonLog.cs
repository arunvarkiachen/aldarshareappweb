﻿using NLog;
using System;


namespace AldarShareApp.Infrastructure
{
    public class CommonLog
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void ControllerEntryLogger(string ControllerName, string Environment, DateTime Date)
        {
            logger.Info("Request entered" + ControllerName + "" + Environment + "" + Date);

        }

        public static void ControllerExitLogger(string ControllerName, string Environment, DateTime Date)
        {
            logger.Info("Request leaving" + ControllerName + "" + Environment + "" + Date);

        }
    }
}