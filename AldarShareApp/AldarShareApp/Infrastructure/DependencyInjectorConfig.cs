﻿using AldarShareApp.DAL.DBContextManager;
using SimpleInjector;
using System.Web.Mvc;
using SimpleInjector.Integration.Web.Mvc;
using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.BLL.ImplementationManager;

namespace AldarShareApp.Infrastructure
{
    public class DependencyInjectorConfig
    {
        /// <summary>
        /// Get the dependecy injector container
        /// </summary>
        /// <returns></returns>
        public static void GetDependencyInjectorContainer()
        {
            var container = new Container();
            container.Register<IDBRepository, DBRepository>();
            container.Register<IVerify, Verify>();
            container.Register<IDivident, Divident>();
            container.Register<IAdmin, Admin>();
            container.Register<ISharePrice, SharePrice>();
            container.Register<IUser, User>();
            container.Register<IAnnualParams, AnnualParams>();
            container.Register<ILTIProgram, LTIProgram>();
            container.Register<IContribution, Contribution>();
            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}