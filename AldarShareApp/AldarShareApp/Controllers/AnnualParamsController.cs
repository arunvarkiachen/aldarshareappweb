﻿using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.Infrastructure;
using AldarShareApp.Models.Constants;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AldarShareApp.Controllers
{
    public class AnnualParamsController : Controller
    {
        private IAnnualParams annualParamsObj { get; set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor Creation
        /// </summary>
        /// <param name="adminObj"></param>
        public AnnualParamsController(IAnnualParams annualParamsObj)
        {
            this.annualParamsObj = annualParamsObj;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            CommonLog.ControllerEntryLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Get Admin Add Awarding View
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddAward()
        {
            CommonLog.ControllerEntryLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Get Admin Add Vesting View
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddVest()
        {
            CommonLog.ControllerEntryLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Add Awarding
        /// </summary>
        /// <param name="addAwarding"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddAwarding(AnnualParamsAwardingRequestModel addAwarding)
        {
            CommonLog.ControllerEntryLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            var user = Session["ASA_User"] as LoggedInUser;
            addAwarding.LoggedInUserID = user != null ? user.ID : 1;
            var result = await annualParamsObj.AddAwarding(addAwarding);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.AwardingSuccess;
            }
            else
            {
                NotificationMessage = Constants.AwardingFailure;
            }
            CommonLog.ControllerExitLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Add Awarding
        /// </summary>
        /// <param name="addVesting"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddVesting(AnnualParamsVestingRequestModel addVesting)
        {
            CommonLog.ControllerEntryLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            var user = Session["ASA_User"] as LoggedInUser;
            addVesting.LoggedInUserID = user != null ? user.ID : 1;
            var result = await annualParamsObj.AddVesting(addVesting);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.VestingSuccess;
            }
            else
            {
                NotificationMessage = Constants.VestingFailure;
            }
            CommonLog.ControllerExitLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Award Details
        /// </summary>
        /// <param name="awardingID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Edit(int awardingID)
        {
            CommonLog.ControllerEntryLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View(await annualParamsObj.GetAwardingDetails(awardingID));
        }

        /// <summary>
        /// Update Awarding
        /// </summary>
        /// <param name="updateAward"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> UpdateAwarding(UpdateAwardingRequestModel updateAward)
        {
            CommonLog.ControllerEntryLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now); var user = Session["ASA_User"] as LoggedInUser;
            updateAward.LoggedInUserID = user != null ? user.ID : 1;
            var result = await annualParamsObj.UpdateAwarding(updateAward);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.AwardingUpdateSuccess;
            }
            else
            {
                NotificationMessage = Constants.AwardingUpdateFailure;
            }
            CommonLog.ControllerExitLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Update Vesting
        /// </summary>
        /// <param name="updateVesting"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> UpdateVesting(UpdateVestingRequestModel updateVesting)
        {
            CommonLog.ControllerEntryLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now); var user = Session["ASA_User"] as LoggedInUser;
            updateVesting.LoggedInUserID = user != null ? user.ID : 1;
            var result = await annualParamsObj.UpdateVesting(updateVesting);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.VestingUpdateSuccess;
            }
            else
            {
                NotificationMessage = Constants.VestingUpdateFailure;
            }
            CommonLog.ControllerExitLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// List Of Admins
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(DatatableRequestModel request)
        {
            CommonLog.ControllerEntryLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("AnnualParamsController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(await annualParamsObj.GetAllAnnualParams(request), JsonRequestBehavior.AllowGet);
        }
    }
}