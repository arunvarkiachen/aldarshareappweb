﻿using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.Infrastructure;
using AldarShareApp.Models.Constants;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.Controllers
{
    public class LTIProgramController : Controller
    {
        private ILTIProgram ltiObjObj { get; set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor Creation
        /// </summary>
        /// <param name="ltiObjObj"></param>
        public LTIProgramController(ILTIProgram ltiObjObj)
        {
            this.ltiObjObj = ltiObjObj;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            CommonLog.ControllerEntryLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Get Admin Add View
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Add()
        {
            CommonLog.ControllerEntryLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Add LTI Program
        /// </summary>
        /// <param name="ltiAdmin"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddLTIProgram(LTIAddRequestModel ltiAdmin)
        {
            CommonLog.ControllerEntryLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now);
            var user = Session["ASA_User"] as LoggedInUser;
            ltiAdmin.LoggedInUserID = user != null ? user.ID : 1;
            var result = await ltiObjObj.AddLTIProgram(ltiAdmin);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.LTISuccess;
            }
            else
            {
                NotificationMessage = Constants.LTIFailure;
            }
            CommonLog.ControllerExitLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get LTI Details
        /// </summary>
        /// <param name="LTIID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Edit(int LTIID)
        {
            CommonLog.ControllerEntryLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View(await ltiObjObj.GetLTIDetails(LTIID));
        }

        /// <summary>
        /// Update LTI
        /// </summary>
        /// <param name="ltiUpdateDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> UpdateAdmin(LTIUpdateRequestModel ltiUpdateDetails)
        {
            CommonLog.ControllerEntryLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now); var user = Session["ASA_User"] as LoggedInUser;
            ltiUpdateDetails.LoggedInUserID = user != null ? user.ID : 1;
            var result = await ltiObjObj.UpdateLTIProgram(ltiUpdateDetails);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.LTIUpdateSuccess;
            }
            else
            {
                NotificationMessage = Constants.LTIUpdateFailure;
            }
            CommonLog.ControllerExitLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// List Of Programs
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(DatatableRequestModel request)
        {
            CommonLog.ControllerEntryLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("LTIController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(await ltiObjObj.GetAllPrograms(request), JsonRequestBehavior.AllowGet);
        }
    }
}