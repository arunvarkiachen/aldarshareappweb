﻿using System.Web.Mvc;

namespace AldarShareApp.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Home
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Dashboard
        /// </summary>
        /// <returns></returns>
        public ActionResult Dashboard()
        {
            return View();
        }
    }
}