﻿using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.Infrastructure;
using AldarShareApp.Models.Constants;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.Controllers
{
    public class AdminController : Controller
    {
        private IAdmin adminObj { get; set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor Creation
        /// </summary>
        /// <param name="adminObj"></param>
        public AdminController(IAdmin adminObj)
        {
            this.adminObj = adminObj;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            CommonLog.ControllerEntryLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Get Admin Add View
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Add()
        {
            CommonLog.ControllerEntryLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Add Admin
        /// </summary>
        /// <param name="adminDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddAdmin(AdminRequestModel adminDetails)
        {
            CommonLog.ControllerEntryLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);           
            var user = Session["ASA_User"] as LoggedInUser;
            adminDetails.userID = user!=null? user.ID : 1;
            var result = await adminObj.AddAdmin(adminDetails);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.AdminSuccess;
            }
            else
            {
                NotificationMessage = Constants.AdminFailure;
            }
            CommonLog.ControllerExitLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Admin Details
        /// </summary>
        /// <param name="AdminID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Edit(int AdminID)
        {
            CommonLog.ControllerEntryLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View(await adminObj.GetAdminDetails(AdminID));
        }

        /// <summary>
        /// Update Admin
        /// </summary>
        /// <param name="adminDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> UpdateAdmin(AdminUpdateRequestModel adminDetails)
        {
            CommonLog.ControllerEntryLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now); var user = Session["ASA_User"] as LoggedInUser;
            adminDetails.LoggedInUserID = user != null ? user.ID : 1;
            var result = await adminObj.UpdateAdmin(adminDetails);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.AdminUpdateSuccess;
            }
            else
            {
                NotificationMessage = Constants.AdminUpdateFailure;
            }
            CommonLog.ControllerExitLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// List Of Admins
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(DatatableRequestModel request)
        {
            CommonLog.ControllerEntryLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(await adminObj.GetAllAdmins(request), JsonRequestBehavior.AllowGet);
        }

       /// <summary>
       /// Download HTML
       /// </summary>
        public void DownloadHTML()
        {
            try
            {
                string url = "http://localhost:55481/admin/index";
                string result = "";
                using (HttpClient client = new HttpClient())
                {
                    using (HttpResponseMessage response = client.GetAsync(url).Result)
                    {
                        using (HttpContent content = response.Content)
                        {
                            result = content.ReadAsStringAsync().Result;
                        }
                    }
                }
                var htmlContent = result;
                var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                var pdfBytes = htmlToPdf.GeneratePdf(htmlContent);
                MemoryStream ms = new MemoryStream(pdfBytes);
                var filename = Guid.NewGuid(); 
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                Response.Buffer = true;
                ms.WriteTo(Response.OutputStream);
                Response.End();
            }
            catch (Exception ex)
            {
                
            }
        }

    }
}