﻿using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.Infrastructure;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.Controllers
{
    public class VerifyController : Controller
    {
        private IVerify verifyObj { get; set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();


        /// <summary>
        /// Constructor Creation
        /// </summary>
        /// <param name="verifyObj"></param>
        public VerifyController(IVerify verifyObj)
        {
            this.verifyObj = verifyObj;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Login Get
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            CommonLog.ControllerEntryLogger("VerifyController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("VerifyController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }



        /// <summary>
        /// Logout
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Logout()
        {
            CommonLog.ControllerEntryLogger("VerifyController", Convert.ToString(Environment.NewLine), DateTime.Now);
            if (Session["ASA_User"] != null)
            {
                LoggedInUser _currentAdmin = (LoggedInUser)Session["ASA_User"];
                await verifyObj.EndUserSession(_currentAdmin.ID, _currentAdmin.SessionID);
                Session.Contents.RemoveAll();
                Session.Clear();
                Session.Abandon();
            }
            CommonLog.ControllerExitLogger("VerifyController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return RedirectToAction("Login", "Verify");
        }
    }
}