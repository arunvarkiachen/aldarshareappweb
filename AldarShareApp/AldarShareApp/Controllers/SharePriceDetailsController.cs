﻿using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.Infrastructure;
using AldarShareApp.Models.RequestModel;
using NLog;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.Controllers
{
    public class SharePriceDetailsController : Controller
    {
        private ISharePrice sharePriceObj { get; set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor Creation
        /// </summary>
        /// <param name="sharePriceObj"></param>
        public SharePriceDetailsController(ISharePrice sharePriceObj)
        {
            this.sharePriceObj = sharePriceObj;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            CommonLog.ControllerEntryLogger("SharePriceController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("SharePriceController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// List Of Share Prices
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(DateTime? start, DatatableRequestModel request, DateTime? end)
        {
            CommonLog.ControllerEntryLogger("SharePriceController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("SharePriceController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(await sharePriceObj.GetAllSharePrices(start,request,end), JsonRequestBehavior.AllowGet);
        }
    }
}