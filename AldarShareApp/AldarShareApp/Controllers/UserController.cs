﻿using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.Infrastructure;
using AldarShareApp.Models.Constants;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.Controllers
{
    public class UserController : Controller
    {
        private IUser userObj { get; set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor Creation
        /// </summary>
        /// <param name="userObj"></param>
        public UserController(IUser userObj)
        {
            this.userObj = userObj;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index(DataTableIndexRequestModel request)
        {
            CommonLog.ControllerEntryLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            DatatableRequestModel _request = new DatatableRequestModel();
            _request.iDisplayLength = request.iDisplayLength==null?10:request.iDisplayLength.Value;
            _request.iDisplayStart = request.iDisplayStart == null?0:request.iDisplayStart.Value;
            _request.iSortCol_0 = request.iSortCol_0 ==null?0:request.iSortCol_0.Value;
            _request.sSortDir_0 = !string.IsNullOrEmpty(request.sSortDir_0)?request.sSortDir_0:"asc";
            _request.sSearch = !string.IsNullOrEmpty(request.sSearch) ? request.sSearch : string.Empty;
            CommonLog.ControllerExitLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View(await userObj.GetAllUsers(_request));
        }

        /// <summary>
        /// Get User Add View
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Add()
        {
            CommonLog.ControllerEntryLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Add User
        /// </summary>
        /// <param name="userDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddAdmin(UserRequestModel userDetails)
        {
            CommonLog.ControllerEntryLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            var user = Session["ASA_User"] as LoggedInUser;
            userDetails.UserID = user != null ? user.ID : 1;
            var result = await userObj.AddUser(userDetails);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.UserSuccess;
            }
            else
            {
                NotificationMessage = Constants.UserFailure;
            }
            CommonLog.ControllerExitLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get User Details
        /// </summary>
        /// <param name="ParticipantID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Edit(int ParticipantID)
        {
            CommonLog.ControllerEntryLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View(await userObj.GetParticipantDetails(ParticipantID));
        }

        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="userDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> UpdateUser(UserUpdateRequestModel userDetails)
        {
            CommonLog.ControllerEntryLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            var user = Session["ASA_User"] as LoggedInUser;
            userDetails.UserID = user != null ? user.ID : 1;
            var result = await userObj.UpdateUser(userDetails);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.UserUpdateSuccess;
            }
            else
            {
                NotificationMessage = Constants.UserUpdateFailure;
            }
            CommonLog.ControllerExitLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// List Of Users
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(DatatableRequestModel request)
        {
            CommonLog.ControllerEntryLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("UserController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(await userObj.GetAllUsers(request), JsonRequestBehavior.AllowGet);
        }
    }
}