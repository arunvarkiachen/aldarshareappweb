﻿using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.Infrastructure;
using AldarShareApp.Models.Constants;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.Controllers
{
    public class ContributionsController : Controller
    {
        private IContribution contributionObj { get; set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor Creation
        /// </summary>
        /// <param name="contributionObj"></param>
        public ContributionsController(IContribution contributionObj)
        {
            this.contributionObj = contributionObj;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            CommonLog.ControllerEntryLogger("ContributionController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("ContributionController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Get Contribution Add View
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Add()
        {
            CommonLog.ControllerEntryLogger("ContributionController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("ContributionController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Add Contribution
        /// </summary>
        /// <param name="contributionDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddContribution(ContributionRequestModel contributionDetails)
        {
            CommonLog.ControllerEntryLogger("ContributionController", Convert.ToString(Environment.NewLine), DateTime.Now);
            var user = Session["ASA_User"] as LoggedInUser;
            contributionDetails.UserID = user != null ? user.ID : 1;
            var result = await contributionObj.AddContribution(contributionDetails);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.ContributionSuccess;
            }
            else
            {
                NotificationMessage = Constants.ContributionFailure;
            }
            CommonLog.ControllerExitLogger("ContributionController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Delete Contribution
        /// </summary>
        /// <param name="ContributionID"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> DeleteContribution(int ContributionID)
        {
            CommonLog.ControllerEntryLogger("ContributionController", Convert.ToString(Environment.NewLine), DateTime.Now); var user = Session["ASA_User"] as LoggedInUser;
            var UserID = user != null ? user.ID : 1;
            var result = await contributionObj.DeleteContribution(ContributionID,UserID);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.ContributionDeleteSuccess;
            }
            else
            {
                NotificationMessage = Constants.ContributionDeleteFailure;
            }
            CommonLog.ControllerExitLogger("ContributionController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// List Of Contributions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(DatatableRequestModel request)
        {
            CommonLog.ControllerEntryLogger("ContributionController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("ContributionController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(await contributionObj.GetAllContributions(request), JsonRequestBehavior.AllowGet);
        }

    }
}