﻿using AldarShareApp.BLL;
using AldarShareApp.BLL.InterfaceManager;
using AldarShareApp.Infrastructure;
using AldarShareApp.Models.Constants;
using AldarShareApp.Models.RequestModel;
using AldarShareApp.Models.ResponseModel;
using NLog;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AldarShareApp.Controllers
{
   // [VerifySession]
    public class DividentController : Controller
    {
        private IDivident dividentObj { get; set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();


        /// <summary>
        /// Constructor Creation
        /// </summary>
        /// <param name="dividentObj"></param>
        public DividentController(IDivident dividentObj)
        {
            this.dividentObj = dividentObj;
        }

        /// <summary>
        /// List Divident Details
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            CommonLog.ControllerEntryLogger("DividentController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("DividentController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Get Divident 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Add()
        {
            CommonLog.ControllerEntryLogger("DividentController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("DividentController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View();
        }

        /// <summary>
        /// Add Divident
        /// </summary>
        /// <param name="SelectedYear"></param>
        /// <param name="DividentValue"></param>
        /// <param name="IsForecasted"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddDivident(DividentRequestModel dividentDetails)
        {
            CommonLog.ControllerEntryLogger("DividentController", Convert.ToString(Environment.NewLine), DateTime.Now);var user = Session["ASA_User"] as LoggedInUser;
            dividentDetails.UserID = user != null ? user.ID : 1;
            var result = await dividentObj.AddDivident(dividentDetails);
            var NotificationMessage = "";
            if(result==true)
            {
                NotificationMessage = Constants.DividentSuccess;
            }
            else
            {
                NotificationMessage = Constants.DividentFailure;
            }
            CommonLog.ControllerExitLogger("DividentController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Divident Details
        /// </summary>
        /// <param name="DividentID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Edit(int DividentID)
        {
            CommonLog.ControllerEntryLogger("DividentController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("DividentController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return View(await dividentObj.GetDividentDetails(DividentID));
        }

        /// <summary>
        /// Update Divident
        /// </summary>
        /// <param name="adminDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> UpdateDivident(DividentUpdateRequestModel dividentDetails)
        {
            CommonLog.ControllerEntryLogger("DividentController", Convert.ToString(Environment.NewLine), DateTime.Now);
            var user = Session["ASA_User"] as LoggedInUser;
            dividentDetails.LoggedInUserID = user != null ? user.ID : 1;
            var result = await dividentObj.UpdateDivident(dividentDetails);
            var NotificationMessage = "";
            if (result == true)
            {
                NotificationMessage = Constants.DividentUpdateSuccess;
            }
            else
            {
                NotificationMessage = Constants.DividentUpdateFailure;
            }
            CommonLog.ControllerExitLogger("DividentController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(NotificationMessage, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// List Of Dividents
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ActionResult> List(DatatableRequestModel request)
        {
            CommonLog.ControllerEntryLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            CommonLog.ControllerExitLogger("AdminController", Convert.ToString(Environment.NewLine), DateTime.Now);
            return Json(await dividentObj.GetAllDividents(request), JsonRequestBehavior.AllowGet);
        }
    }
}