﻿

namespace AldarShareApp.Models.ResponseModel
{
    public class NotificationResponseModel
    {
        public string Type { get; set; }
        public string Text { get; set; }

        public NotificationResponseModel(string Type, string Text)
        {
            this.Text = Text;
            this.Type = Type;
        }
    }
}
