﻿

namespace AldarShareApp.Models.ResponseModel
{
    public class LoggedInUser
    {
        public string Username { get; set; }

        public string Name { get; set; }

        public int ID { get; set; }

        public string Image { get; set; }

        public string Role { get; set; }
        public int SessionID { get; set; }
    }
}
