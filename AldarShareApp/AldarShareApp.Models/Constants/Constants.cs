﻿

namespace AldarShareApp.Models.Constants
{
    public static class Constants
    {
        #region Messages

        public static readonly string DividentSuccess = "Divident Created Successfully";
        public static readonly string DividentFailure = "Divident Creation Failed";
        public static readonly string AdminSuccess = "Admin Created Successfully";
        public static readonly string AdminFailure = "Admin Creation Failed";
        public static readonly string AdminUpdateSuccess = "Admin Updated Successfully";
        public static readonly string AdminUpdateFailure = "Admin Updation Failed";
        public static readonly string DividentUpdateSuccess = "Divident Updated Successfully";
        public static readonly string DividentUpdateFailure = "Divident Updation Failed";
        public static readonly string UserSuccess = "User Created Successfully";
        public static readonly string UserFailure = "User Creation Failed";
        public static readonly string UserUpdateSuccess = "User Updated Successfully";
        public static readonly string UserUpdateFailure = "User Updation Failed";
        public static readonly string AwardingSuccess = "Awarding added Successfully";
        public static readonly string AwardingFailure = "Awarding Creation Failed";
        public static readonly string VestingSuccess = "Vesting added Successfully";
        public static readonly string VestingFailure = "Vesting Creation Failed";
        public static readonly string AwardingUpdateSuccess = "Awarding Updated Successfully";
        public static readonly string AwardingUpdateFailure = "Awarding Updation Failed";
        public static readonly string VestingUpdateSuccess = "Vesting Updated Successfully";
        public static readonly string VestingUpdateFailure = "Vesting Updation Failed";
        public static readonly string LTISuccess = "LTI Created Successfully";
        public static readonly string LTIFailure = "LTI Creation Failed";
        public static readonly string LTIUpdateSuccess = "LTI Updated Successfully";
        public static readonly string LTIUpdateFailure = "LTI Updation Failed";
        public static readonly string ContributionSuccess = "Contribution Created Successfully";
        public static readonly string ContributionFailure = "Contribution Creation Failed";
        public static readonly string ContributionDeleteSuccess = "Contribution Deleted Successfully";
        public static readonly string ContributionDeleteFailure = "Contribution Deleted Failed";

        #endregion

        #region Roles

        public const string AdministratorsRole = "Admin";

        #endregion

    }
}
