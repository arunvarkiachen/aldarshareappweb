﻿

namespace AldarShareApp.Models.CommonModel
{
    public class AdminCommonModel
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public bool IsActive { get; set; }
        public int ID { get; set; }
    }
}
