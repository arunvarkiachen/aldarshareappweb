﻿

namespace AldarShareApp.Models.CommonModel
{
    public class LTISegmentCommonModel
    {
        public string AwardingUnit { get; set; }
        public int VestingYear { get; set; }
        public decimal percentage { get; set; }
        public int PaymentYear { get; set; }
        public string PaymentMonth { get; set; }
        public int ProgramID { get; set; }
        public int SegmentID { get; set; }//for update only
    }
}
