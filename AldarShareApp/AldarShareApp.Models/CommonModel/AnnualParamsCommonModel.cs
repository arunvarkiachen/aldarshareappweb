﻿using System;
namespace AldarShareApp.Models.CommonModel
{
    public class AnnualParamsCommonModel
    {
        public int ID { get; set; }
        public int Year { get; set; }
        public DateTime AwardingStartDate { get; set; }
        public DateTime AwardingEndDate { get; set; }
        public string AwardingShareValue { get; set; }
        public DateTime VestingStartDate { get; set; }
        public DateTime VestingEndDate { get; set; }
        public string VestingShareValue { get; set; }
        public bool isForcastedVSP { get; set; }
    }
}
