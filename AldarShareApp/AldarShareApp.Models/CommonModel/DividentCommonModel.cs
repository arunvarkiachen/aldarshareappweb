﻿
namespace AldarShareApp.Models.CommonModel
{
    public class DividentCommonModel
    {
        public int Year { get; set; }
        public string Divident { get; set; }
        public bool IsForecast { get; set; }
        public int ID { get; set; }
    }
}
