﻿using System.Collections.Generic;

namespace AldarShareApp.Models.CommonModel
{
   public class ListofSegmentsCommonModel
    {
        public List<LTISegmentCommonModel> SegmentsFullList { get; set; }
    }
}
