﻿

using System;

namespace AldarShareApp.Models.CommonModel
{
    public class ParticipantCommonModel
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string EmployeeCode { get; set; }
        public string Designation { get; set; }
        public bool IsActiveParticipant { get; set; }
        public DateTime ParticipationStartDate { get; set; }
        public DateTime ParticipationEndDate { get; set; }
        public int ID { get; set; }
    }
}
