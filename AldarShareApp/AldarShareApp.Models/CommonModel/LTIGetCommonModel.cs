﻿using System.Collections.Generic;

namespace AldarShareApp.Models.CommonModel
{
    public class LTIGetCommonModel
    {
        public int ID { get; set; }
        public int Year { get; set; }
        public string ProgramTitle { get; set; }
        public List<LTISegmentCommonModel> SegmentsList { get; set; }
    }
}
