﻿using System.ComponentModel.DataAnnotations;

namespace AldarShareApp.Models.RequestModel
{
    public class DividentUpdateRequestModel
    {
        [Required]
        public int SelectedYear { get; set; }
        [Required]
        public string DividentValue { get; set; }
        public bool IsForecasted { get; set; }
        public int DividentID { get; set; }
        public int LoggedInUserID { get; set; }
    }
}
