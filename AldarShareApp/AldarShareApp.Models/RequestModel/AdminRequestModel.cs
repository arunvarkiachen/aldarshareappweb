﻿

using System.ComponentModel.DataAnnotations;

namespace AldarShareApp.Models.RequestModel
{
    public class AdminRequestModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string UserName { get; set; }
        public int userID { get; set; }
        public string Designation { get; set; }
        public string EmployeeCode { get; set; }
    }
}
