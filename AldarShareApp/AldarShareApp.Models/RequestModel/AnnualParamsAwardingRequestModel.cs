﻿

using System;

namespace AldarShareApp.Models.RequestModel
{
    public class AnnualParamsAwardingRequestModel
    {
        public int year { get; set; }
        public DateTime AwardingStartDate { get; set; }
        public DateTime AwardingEndDate { get; set; }
        public string AwrdingSharePrice { get; set; }
        public bool IsAwardingForward { get; set; }
        public int LoggedInUserID { get; set; }
        public int Duration { get; set; }

      
    }
}
