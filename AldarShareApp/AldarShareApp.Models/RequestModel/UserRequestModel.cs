﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace AldarShareApp.Models.RequestModel
{
    public class UserRequestModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string EmployeeCode { get; set; }
        [Required]
        public string Designation { get; set; }
        [Required]
        public DateTime ParticipationStartDate { get; set; }
        public int UserID { get; set; }
    }
}
