﻿
using System.ComponentModel.DataAnnotations;

namespace AldarShareApp.Models.RequestModel
{
    public class DividentRequestModel
    {
        [Required]
        public int SelectedYear { get; set; }
        [Required]
        public string DividentValue { get; set; }
        public bool IsForecasted { get; set; }
        public int UserID { get; set; }
    }
}
