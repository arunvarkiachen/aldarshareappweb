﻿

namespace AldarShareApp.Models.RequestModel
{
    public class ContributionRequestModel
    {
        public int Year { get; set; }
        public int PID { get; set; }
        public int EmpID { get; set; }
        public string Bonus { get; set; }
        public decimal EmpPercentage { get; set; }
        public decimal CompPercentage { get; set; }
        public int UserID { get; set; }
        public decimal CurrentShareValue { get; set; }
    }
}
