﻿using System;

namespace AldarShareApp.Models.RequestModel
{
    public class UpdateVestingRequestModel
    {
        public int Year { get; set; }
        public DateTime VestingStartDate { get; set; }
        public DateTime VestingEndDate { get; set; }
        public string VestingSharePrice { get; set; }
        public bool IsVesting { get; set; }
        public bool IsForecastedVesting { get; set; }
        public string ForecastValue { get; set; }
        public int LoggedInUserID { get; set; }
        public bool IsForward { get; set; }
        public int Duration { get; set; }
        public int VestingID { get; set; }
    }
}
