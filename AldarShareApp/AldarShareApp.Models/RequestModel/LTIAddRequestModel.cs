﻿

using AldarShareApp.Models.CommonModel;
using System.Collections.Generic;

namespace AldarShareApp.Models.RequestModel
{
    public class LTIAddRequestModel
    {
        public int Year { get; set; }
        public string ProgramTitle { get; set; }
        public List<LTISegmentCommonModel> SegmentsList { get; set; }
        public int LoggedInUserID { get; set; }
        public int StatusID { get; set; }
    }
}
