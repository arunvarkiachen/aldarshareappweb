﻿using System.ComponentModel.DataAnnotations;

namespace AldarShareApp.Models.RequestModel
{
 
    public class AdminUpdateRequestModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string UserName { get; set; }
        public int AdminID { get; set; }
        [Required]
        public bool IsActive { get; set; }
        public int LoggedInUserID { get; set; }
        public string Designation { get; set; }
        public string EmpCode { get; set; }
    }
}
