//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AldarShareApp.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class LTIAdminDet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LTIAdminDet()
        {
            this.UserSessions = new HashSet<UserSession>();
        }
    
        public int id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public Nullable<bool> isActive { get; set; }
        public string createdUser { get; set; }
        public Nullable<System.DateTime> createdOn { get; set; }
        public string updatedUser { get; set; }
        public Nullable<System.DateTime> updatedOn { get; set; }
        public string employeeCode { get; set; }
        public string jobTitle { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserSession> UserSessions { get; set; }
    }
}
