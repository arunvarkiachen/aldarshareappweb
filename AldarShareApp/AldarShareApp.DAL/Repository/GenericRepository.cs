﻿using System;
using System.Collections.Generic;
using System.Linq;
using AldarShareApp.DAL.Entity;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;

namespace AldarShareApp.DAL.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private AldarEmployeeShareAppEntities db = null;
        private DbSet<T> table = null;

        public GenericRepository()
        {
            this.db = new AldarEmployeeShareAppEntities();
            table = db.Set<T>();
        }

        public GenericRepository(AldarEmployeeShareAppEntities db)
        {
            this.db = db;
            table = db.Set<T>();
        }


        public Task<T> GetById(int id) => db.Set<T>().FindAsync(id);

        public Task<T> FirstOrDefault(Expression<Func<T, bool>> predicate)
            => db.Set<T>().FirstOrDefaultAsync(predicate);

        public async Task Add(T entity)
        {
            table.Add(entity);
            await db.SaveChangesAsync();
        }

        public Task Update(T entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            return db.SaveChangesAsync();
        }

        public Task Remove(T entity)
        {
            db.Set<T>().Remove(entity);
            return db.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await db.Set<T>().ToListAsync();
        }

        public async Task<IEnumerable<T>> Where(Expression<Func<T, bool>> predicate)
        {
            return await db.Set<T>().Where(predicate).ToListAsync();
        }

        public Task<int> CountAll() => db.Set<T>().CountAsync();

        public Task<int> CountWhere(Expression<Func<T, bool>> predicate)
            => db.Set<T>().CountAsync(predicate);

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

