﻿using AldarShareApp.DAL.Entity;

namespace AldarShareApp.DAL.DBContextManager
{
    public class DBRepository: IDBRepository
    {
        private AldarEmployeeShareAppEntities entities = null;
        public DBRepository()
        {

        }
        public AldarEmployeeShareAppEntities Get()
        {
            if (entities == null)
            {
                entities = new AldarEmployeeShareAppEntities();
            }
            return entities;
        }
    }
}
