﻿using AldarShareApp.DAL.Entity;

namespace AldarShareApp.DAL.DBContextManager
{
    public interface IDBRepository
    {
        AldarEmployeeShareAppEntities Get();
    }
}
